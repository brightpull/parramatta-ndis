<?php get_header(); ?>

<div class="h-64 lg:hidden bg-center bg-cover" style="background-image: url(<?php echo bedrock_hero_img_url(); ?>);"></div>

<div class="hero" style="background-image: url(<?php echo bedrock_hero_img_url(); ?>);">

	<div class="hero__blue"></div>

	<div class="hero__red bg-cover" style="background-image: url(<?php echo get_template_directory_uri(); ?>/svg/red-angle-flip.svg); background-repeat: no-repeat;"></div>

	<div class="hero__content">

		<div class="container">

			<h1 class="text-4xl lg:text-5xl leading-tight mb-4">
				<?php echo get_the_title( bedrock_ancestor_id() ); ?>
			</h1>

			<div class="bg-white w-32" style="height: 2px;"></div>

		</div>

	</div>

</div>

<div class="bg-white py-16">

	<div class="container grid-sidebar">

		<div class="content-area">

			<?php if ( have_posts() ) : ?>

			    <?php while ( have_posts() ) : the_post(); ?>

			        <?php if ($post->post_parent) : ?>
						<h2 class="text-blue"><?php the_title(); ?></h2>
					<?php endif; ?>

			        <?php the_content(); ?>

			    <?php endwhile; ?>

			<?php endif; ?>

		</div>

		<div>

			<div class="bg-shade rounded-lg py-12 px-6 text-center mb-6">

				<p class="text-lg font-semibold">Already have an NDIS plan and would like to speak to us about how we can help support you?</p>

				<a class="button bg-red hover:bg-red_dark" href="<?php echo get_permalink( 18 ); ?>">Get In Touch</a>

			</div>

			<div style="height: 25px;"></div>
			<div style="height: 25px;"></div>

			<?php if ( have_rows('what_we_do_columns', 8) ) : ?>

				<div class="bg-shade rounded-lg py-12 px-6 text-center text-sm relative">

					<h4 class="bg-blue font-medium text-white inline-block rounded-lg absolute top-0 px-6 py-4 uppercase text-center text-xl" style="left: 50%; width: 200px; margin-left: -100px; margin-top: -25px; z-index: 999;">
						What We Do
						<img src="<?php echo get_template_directory_uri(); ?>/svg/blue-triangle.svg" alt="blue triangle" class="absolute" style="left: 50%; width: 24px; margin-left: -12px; bottom: -10px;">
					</h4>

					<div style="height: 25px;"></div>

					<div class="what_we_do_carousel" style="width: 311px;">

						<?php while ( have_rows('what_we_do_columns', 8) ) : the_row(); ?>

							<div>

								<h4 class="text-xl text-red svg">
									<img src="<?php echo get_template_directory_uri(); ?>/svg/<?php the_sub_field('icon'); ?>.svg" alt="icon">
									<?php the_sub_field('title'); ?>
								</h4>
								<p><?php the_sub_field('paragraph'); ?></p>
								<a class="button bg-red hover:bg-red_dark" href="<?php the_sub_field('button_link'); ?>">Find out more</a>

							</div>

						<?php endwhile; ?>

					</div>

				</div>

			<?php endif; ?>

		</div>

	</div>

</div>

<div class="bg-white pb-16">

	<div class="container">

		<?php echo get_template_part( 'parts/already' ); ?>

	</div>

</div>

<?php get_footer(); ?>
