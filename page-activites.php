<?php

/* Template Name: Activities */

get_header(); ?>

<div class="h-64 lg:hidden bg-center bg-cover" style="background-image: url(<?php echo bedrock_hero_img_url(); ?>);"></div>

<div class="hero" style="background-image: url(<?php echo bedrock_hero_img_url(); ?>);">

	<div class="hero__blue"></div>

	<div class="hero__red bg-cover" style="background-image: url(<?php echo get_template_directory_uri(); ?>/svg/red-angle-flip.svg); background-repeat: no-repeat;"></div>

	<div class="hero__content">

		<div class="container">

			<h1 class="text-4xl lg:text-5xl leading-tight mb-4">
				What We Do
			</h1>

			<div class="bg-white w-32" style="height: 2px;"></div>

		</div>

	</div>

</div>

<div class="bg-white py-16">

	<div class="container text-center lg:w-2/3">

		<h2><?php the_title(); ?></h2>

		<?php the_field('page_introduction'); ?>

	</div>

	<div class="container grid-4" style="grid-gap: 1rem;">

		<?php query_posts( 'post_type=activity&posts_per_page=20' ); ?>

		<?php if ( have_posts() ) : ?>

		    <?php while ( have_posts() ) : the_post(); ?>

				<a class="block" href="<?php echo get_permalink(); ?>">

					<div class="bg-center bg-cover rounded-t-lg" style="background-image: url(<?php echo get_field( 'thumbnail' ); ?>); height: 175px;"></div>

					<?php

					$category = get_field_object('category');
					$value = $category['value'];
					$label = $category['choices'][ $value ];

					?>

					<div class="py-6 px-4 bg-blue text-white flex items-center">

						<img class="h-8 w-8 mr-4" src="<?php echo get_template_directory_uri(); ?>/svg/category-<?php echo $value; ?>.svg" alt="group icon">

						<div>

							<h4 class="mb-0 text-base"><?php the_title(); ?></h4>

							<p class="text-xs mb-0"><?php echo $label; ?></p>

						</div>

					</div>

				</a>

		    <?php endwhile; ?>

		<?php endif; ?>

		<?php wp_reset_query(); ?>

	</div>

</div>

<div class="py-24 lg:py-48 bg-center bg-cover" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)),url(<?php the_field('video_image', 40); ?>);">

	<div class="container text-center">

		<p class="font-medium leading-tight text-white text-4xl lg:text-5xl">
			<?php the_field('video_text', 40); ?>
		</p>

		<a class="button bg-red hover:bg-red_dark jq-youtube" href="#" target="_blank">
			<?php the_field('video_button', 40); ?>
		</a>

	</div>

</div>

<div class="youtube__wrap fixed inset-0 z-40">
	<a class="jq-youtube" href="#">
		✕
	</a>
	<div class="youtube__modal">
		<div class="youtube__box">
			<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe id="youtube-video" src='https://www.youtube.com/embed/<?php the_field('video_id', 40); ?>' frameborder='0' allowfullscreen></iframe></div>
		</div>
	</div>
</div>

<div class="bg-white py-16">

	<div class="container">

		<?php echo get_template_part( 'parts/already' ); ?>

	</div>

</div>

<?php get_footer(); ?>
