<?php get_header(); ?>

<div class="h-64 lg:hidden bg-center bg-cover" style="background-image: url(<?php echo bedrock_hero_img_url(); ?>);"></div>

<div class="hero" style="background-image: url(<?php echo bedrock_hero_img_url(); ?>);">

	<div class="hero__blue"></div>

	<div class="hero__red bg-cover" style="background-image: url(<?php echo get_template_directory_uri(); ?>/svg/red-angle-flip.svg); background-repeat: no-repeat;"></div>

	<div class="hero__content">

		<div class="container">

			<h1 class="text-4xl lg:text-5xl leading-tight mb-4">
				Group Activities
			</h1>

			<div class="bg-white w-32" style="height: 2px;"></div>

		</div>

	</div>

</div>

<div class="bg-white py-16">

	<div class="container">

		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<h2 class="mb-2 text-blue"><?php the_title(); ?></h2>

				<?php

				$category = get_field_object('category');
				$value = $category['value'];
				$label = $category['choices'][ $value ];

				?>

				<div class="flex items-center mb-6">

					<img class="h-6 w-6 mr-3" src="<?php echo get_template_directory_uri(); ?>/svg/category-<?php echo $value; ?>-red.svg" alt="group icon">

					<p class="text-lg text-red mb-0"><?php echo $label; ?></p>

				</div>

				<div class="grid-sidebar">

					<div>

				        <?php the_content(); ?>

						<div class="h-3"></div>

						<?php $gallery_images = get_field( 'gallery' ); ?>

						<?php if ( $gallery_images ) : ?>
							<h4 class="text-blue uppercase">Gallery</h4>
							<div class="grid-gallery">
								<?php foreach( $gallery_images as $image ): ?>
									<a class="" href="<?php echo wp_get_attachment_image_src($image['ID'], 'full')[0]; ?>">
										<img src="<?php echo wp_get_attachment_image_src($image['ID'], 'thumbnail')[0]; ?>" alt="<?php echo wp_get_attachment_caption($image['ID']); ?>">
									</a>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>

					</div>

					<div>

						<div class="bg-shade rounded-lg py-10 px-6 text-sm mb-6">

							<h4 class="text-xl text-red svg justify-start">
								<img src="<?php echo get_template_directory_uri(); ?>/svg/icon-calendar.svg" alt="calendar icon">
								Where and when?
							</h4>

							<p class="mb-0"><?php echo get_field( 'where_and_when' ); ?></p>

						</div>

						<div class="bg-shade rounded-lg py-10 px-6 text-sm">

							<h4 class="text-xl text-red svg justify-start">
								<img src="<?php echo get_template_directory_uri(); ?>/svg/icon-star.svg" alt="star icon">
								Join this activity
							</h4>

							<p>Existing clients can sign up for this activity using the button below. Not yet a client? Get in touch with us to get started.</p>

							<div class="text-center">
								<a class="button bg-red hover:bg-red_dark jq-activity-box" href="#">Join this activity</a>
							</div>

						</div>

					</div>

				</div>

			<?php endwhile; ?>

		<?php endif; ?>

		<div class="bg-gray-600 mt-16" style="height: 1px;">

		</div>

	</div>

</div>

<div class="bg-white">

	<?php echo get_template_part( 'parts/activity-slick' ); ?>

</div>

<div class="bg-white py-16">

	<div class="container">

		<?php echo get_template_part( 'parts/already' ); ?>

	</div>

</div>

<div class="activity-box__wrap fixed inset-0 z-40">
	<div class="activity-box__modal">
		<div class="activity-box__box">
			<div class="flex items-center justify-between mb-6 text-white">
				<h3 class="mb-0">Join this activity</h3>
				<a class="jq-activity-box text-lg" href="#">
					✕
				</a>
			</div>
			<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true]'); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
