<?php

/* Template Name: Contact */

get_header(); ?>

<div class="bg-gray-100 py-16">

	<div class="container" style="max-width: 800px;">

		<div class="text-center">
			<h1 class="text-blue mb-12"><?php the_title(); ?></h1>
		</div>

		<div>

			<?php if ( have_posts() ) : ?>

			    <?php while ( have_posts() ) : the_post(); ?>

			        <?php the_content(); ?>

			    <?php endwhile; ?>

			<?php endif; ?>

		</div>

	</div>

</div>

<?php get_footer(); ?>
