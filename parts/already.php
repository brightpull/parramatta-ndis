<div class="bg-shade p-6 rounded-lg lg:flex lg:items-center lg:justify-between text-center">

	<img class="inline-block mb-2 lg:mb-0 h-10 w-10" src="<?php echo get_template_directory_uri(); ?>/svg/icon-speech-blue.svg" alt="comment bubble icon">

	<p class="font-semibold text-lg mb-0 lg:leading-none mb-4 lg:mb-0">Already have an NDIS plan and would like to speak to us about how we can support you?</p>

	<a class="button bg-blue hover:bg-blue_dark" href="<?php echo get_permalink( 18 ); ?>">Get In Touch</a>

</div>
