// Animate on scroll
AOS.init();

// Slick carousel (activities)
jQuery(document).ready(function(){
	jQuery('.activity-slick').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		prevArrow: '<button type="button" class="slick-prev slick-arrow">&lsaquo;</button>',
		nextArrow: '<button type="button" class="slick-next slick-arrow">&rsaquo;</button>',
		responsive: [
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
		],
	});
});

jQuery(document).ready(function(){
  jQuery('.what_we_do_carousel').slick({
	  infinite: true,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  dots: true,
  });
});

// YouTube popup
jQuery('.jq-youtube').click(function(e) {
	e.preventDefault();
	jQuery('.youtube__wrap').fadeToggle();
	jQuery('#youtube-video').attr('src', jQuery('iframe').attr('src'));
});

// Slidedown menus
$('.has-slidedown').click(function(e) {
    e.preventDefault();
    $(this).siblings('.slidedown').slideToggle();
});

// Dropdown menus
$('.has-dropdown').click(function(e) {
    e.preventDefault();
    $(this).siblings('.dropdown').toggle();
});
$('.has-dropdown').mouseout(function() {
    $(this).siblings('.dropdown').hide();
});
$('.dropdown').mouseover(function() {
    $(this).show();
});
$('.dropdown').mouseout(function() {
    $(this).hide();
});

// Newsletter signup
jQuery('.jq-activity-box').click(function(e) {
	e.preventDefault();
	jQuery('.activity-box__wrap').fadeToggle();
});

// Mobile menu
jQuery('.js-mobile-menu').click(function(e) {
	jQuery('.mobile-menu').slideToggle();
});

// Lightbox
jQuery(document).ready(function() {
    jQuery('.grid-gallery a').simpleLightbox({
        captionsData: 'alt'
    });
});

// Google Translate
function googleTranslateElementInit() {
     new google.translate.TranslateElement(
                {pageLanguage: 'en'},
                'google_translate_element'
            );
        /*
        To remove the "powered by google",
        uncomment one of the following code blocks.
        NB: This breaks Google's Attribution Requirements:
        https://developers.google.com/translate/v2/attribution#attribution-and-logos
    */

    // Native (but only works in browsers that support query selector)
    if(typeof(document.querySelector) == 'function') {
        document.querySelector('.goog-logo-link').setAttribute('style', 'display: none');
        document.querySelector('.goog-te-gadget').setAttribute('style', 'font-size: 0');
    }

    // If you have jQuery - works cross-browser - uncomment this
    jQuery('.goog-logo-link').css('display', 'none');
    jQuery('.goog-te-gadget').css('font-size', '0');
}
