<?php get_header(); ?>

<div class="h-64 lg:hidden bg-center bg-cover" style="background-image: url(<?php echo bedrock_hero_img_url(); ?>);"></div>

<div class="hero" style="background-image: url(<?php echo bedrock_hero_img_url(); ?>);">

	<div class="hero__blue"></div>

	<div class="hero__red bg-cover" style="background-image: url(<?php echo get_template_directory_uri(); ?>/svg/red-angle-flip.svg); background-repeat: no-repeat;"></div>

	<div class="hero__content">

		<div class="container">

			<h1 class="text-4xl lg:text-5xl leading-tight mb-4">
				News
			</h1>

			<div class="bg-white w-32" style="height: 2px;"></div>

		</div>

	</div>

</div>

<div class="bg-white py-16">

	<div class="container grid-sidebar">

		<div class="content-area">

			<?php if ( have_posts() ) : ?>

			    <?php while ( have_posts() ) : the_post(); ?>

					<h2 class="text-blue mb-1"><?php the_title(); ?></h2>

					<p class="text-sm"><?php the_time('F j, Y'); ?></p>

			        <?php the_content(); ?>

			    <?php endwhile; ?>

			<?php endif; ?>

		</div>

		<div>

			<?php

			$args = array (
				'posts_per_page' => '2',
				'post__not_in' => array(get_the_ID())
			);

			$query = new WP_Query( $args );

			?>

			<?php if ( $query->have_posts() ) : ?>

				<h4>Recent News</h4>

			    <?php while ( $query->have_posts() ) : $query->the_post(); ?>

					<div>

						<a class="bg-center bg-cover mb-6 block" href="<?php echo get_permalink(); ?>" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>); height: 175px;"></a>

						<h4 class="text-xl mb-1"><a class="text-blue" href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>

						<p class="text-sm"><?php the_time('F j, Y'); ?></p>

						<p class="text-xs mb-0"><?php the_excerpt(); ?></p>

						<a class="button bg-red hover:bg-red_dark inline-block mb-12" href="<?php echo get_permalink(); ?>">Read more</a>

					</div>

			    <?php endwhile; ?>

			<?php endif; ?>

			<?php wp_reset_query(); ?>

		</div>

	</div>

</div>

<div class="bg-white pb-16">

	<div class="container">

		<?php echo get_template_part( 'parts/already' ); ?>

	</div>

</div>

<?php get_footer(); ?>
