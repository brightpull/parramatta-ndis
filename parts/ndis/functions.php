<?php

get_template_part( 'includes/cleanup' );
get_template_part( 'includes/cpt' );
get_template_part( 'includes/helpers' );
get_template_part( 'includes/shortcodes' );

// Add theme support
add_theme_support( 'post-thumbnails' );
add_theme_support( 'title-tag' );

// Enqueue scripts
function bedrock_load_assets()
{
    // jQuery
    if ( !is_admin() ) { wp_deregister_script( 'jquery' ); }
    wp_enqueue_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), '3.3.1', false );

    // CSS
    wp_enqueue_style( 'aos', 'https://unpkg.com/aos@next/dist/aos.css' );
    wp_enqueue_style( 'simple-lightbox', get_template_directory_uri() . '/css/simplelightbox.min.css', array(), '1.17.2' );
    wp_enqueue_style( 'app', get_template_directory_uri() . '/style.css' );

    // JavaScript
    wp_enqueue_script( 'aos', 'https://unpkg.com/aos@next/dist/aos.js', array(), '3.0.0', true );
    wp_enqueue_script( 'simple-lightbox', get_template_directory_uri() . '/js/simple-lightbox.min.js', array(), '1.17.2', true );
    wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.min.js', array(), '1.8.1', true );
    wp_enqueue_script( 'app', get_template_directory_uri() . '/js/app-min.js', array(), null, true );
}
add_action( 'wp_enqueue_scripts', 'bedrock_load_assets' );
