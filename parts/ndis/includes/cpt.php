<?php

function custom_post_types()
{
    $labels = array(
        'name'               => 'Activities',
        'singular_name'      => 'Activity',
        'menu_name'          => 'Activities',
        'name_admin_bar'     => 'Activity',
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New Activity',
        'new_item'           => 'New Activity',
        'edit_item'          => 'Edit Activity',
        'view_item'          => 'View Activity',
        'all_items'          => 'All Activities',
        'search_items'       => 'Search Activities',
        'parent_item_colon'  => 'Parent Activity',
        'not_found'          => 'No Activities Found',
        'not_found_in_trash' => 'No Activities Found in Trash'
    );

    $args = array(
        'labels'              => $labels,
        'public'              => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_nav_menus'   => true,
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-buddicons-groups',
        'capability_type'     => 'post',
        'hierarchical'        => false,
        'supports'            => array( 'title', 'editor' ),
        'has_archive'         => true,
        'rewrite'             => array( 'slug' => 'activity' ),
        'query_var'           => true
    );

    register_post_type( 'activity', $args );
}
add_action( 'init', 'custom_post_types' );
