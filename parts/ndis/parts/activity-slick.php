<div class="bg-white py-4">

	<div class="container">

		<?php query_posts( 'post_type=activity&posts_per_page=60' ); ?>

		<?php if ( have_posts() ) : ?>
			<div class="activity-slick">
			<?php while ( have_posts() ) : the_post(); ?>

				<a class="block mx-2" href="<?php echo get_permalink(); ?>">

					<div class="bg-center bg-cover rounded-t-lg" style="background-image: url(<?php echo get_field( 'thumbnail' ); ?>); height: 175px;"></div>

					<?php

					$category = get_field_object('category');
					$value = $category['value'];
					$label = $category['choices'][ $value ];

					?>

					<div class="py-6 px-4 bg-blue text-white flex items-center">

						<img class="h-8 w-8 mr-4" src="<?php echo get_template_directory_uri(); ?>/svg/category-<?php echo $value; ?>.svg" alt="group icon">

						<div>

							<h4 class="mb-0 text-base"><?php the_title(); ?></h4>

							<p class="text-xs mb-0"><?php echo $label; ?></p>

						</div>

					</div>

				</a>

			<?php endwhile; ?>
			</div>
		<?php endif; ?>

		<?php wp_reset_query(); ?>

	</div>

</div>
