<h4 class="bg-blue font-medium text-white inline-block rounded-lg absolute top-0 px-6 py-4 uppercase text-center text-xl" style="left: 50%; width: 200px; margin-left: -100px; margin-top: -25px;">
	What We Do
	<img src="<?php echo get_template_directory_uri(); ?>/svg/blue-triangle.svg" alt="blue triangle" class="absolute" style="left: 50%; width: 24px; margin-left: -12px; bottom: -10px;">
</h4>
