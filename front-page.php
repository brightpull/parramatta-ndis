<?php get_header(); ?>

<div class="h-64 lg:hidden bg-center bg-cover" style="background-image: url(<?php echo bedrock_hero_img_url(); ?>);"></div>

<div class="hero" style="background-image: url(<?php echo bedrock_hero_img_url(); ?>);">

	<div class="hero__blue"></div>

	<div class="hero__red bg-cover" style="background-image: url(<?php echo get_template_directory_uri(); ?>/svg/red-angle-flip.svg); background-repeat: no-repeat;"></div>

	<div class="hero__content">

		<div class="container">

			<h1 class="text-4xl lg:text-5xl leading-tight mb-4">Let us help you <br>achieve your goals</h1>

			<div class="bg-white w-32 mb-6" style="height: 2px;"></div>

			<p class="uppercase text-sm font-medium mb-0">Empowering you on your recovery journey</p>

		</div>

	</div>

</div>

<div class="bg-white py-16">

	<div class="container text-center lg:w-2/3">

		<p class="mb-12 text-2xl">
			<?php the_field('intro', 8); ?>
		</p>

	</div>

	<div class="container">

		<div class="bg-shade p-6 mb-16 rounded-lg lg:flex lg:items-center lg:justify-between text-center">

			<img class="inline-block mb-2 lg:mb-0 h-10 w-10" src="<?php echo get_template_directory_uri(); ?>/svg/icon-speech-red.svg" alt="comment bubble icon">

			<p class="font-semibold text-lg mb-0 lg:leading-none mb-4 lg:mb-0">Already have an NDIS plan and would like to speak to us about how we can support you?</p>

			<a class="button bg-red hover:bg-red_dark" href="<?php echo get_permalink( 18 ); ?>">Get In Touch</a>

		</div>

	</div>

	<div class="container lg:flex lg:relative">

		<div class="what-is__image bg-center bg-cover" style="background-image: url(<?php the_field('box_image', 8); ?>);">

		</div>

		<div class="what-is__box bg-red text-white p-6 lg:px-12 lg:pt-24 lg:pb-16 lg:flex lg:items-center lg:absolute lg:right-0 bg-top" style="background-image: url(<?php echo get_template_directory_uri(); ?>/svg/red-angle.svg); background-repeat: no-repeat;">

			<div>

				<?php the_field('box_content', 8); ?>

			</div>

		</div>

	</div>

	<div style="height: 25px;"></div>

</div>

<div class="bg-shade py-16 relative">

	<?php echo get_template_part( 'parts/what-we-do' ); ?>

	<div class="container container--wide grid-5 text-center">

		<?php if ( have_rows('what_we_do_columns', 8) ) : ?>

            <?php while ( have_rows('what_we_do_columns', 8) ) : the_row(); ?>

				<div>

					<div class="lg:hidden" style="height: 25px;"></div>

					<img class="inline-block h-12 w-12 mb-6" src="<?php echo get_template_directory_uri(); ?>/svg/<?php the_sub_field('icon'); ?>.svg" alt="icon">

					<h4 class="text-red">
						<?php the_sub_field('title'); ?>
					</h4>

					<p class="text-sm">
						<?php the_sub_field('paragraph'); ?>
					</p>

					<a class="button--border border border-red hover:bg-red hover:text-white" href="<?php the_sub_field('button_link'); ?>" style="color: ">
						Find out more
					</a>

				</div>

				<div class="h-1 lg:hidden"></div>

            <?php endwhile; ?>

        <?php endif; ?>

	</div>

</div>

<div class="bg-white py-16">

	<?php echo get_template_part( 'parts/activity-slick' ); ?>

</div>

<div class="py-24 lg:py-48 bg-center bg-cover" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)),url(<?php the_field('video_image', 8); ?>);">

	<div class="container text-center">

		<p class="font-medium leading-tight text-white text-4xl lg:text-5xl">
			<?php the_field('video_text', 8); ?>
		</p>

		<a class="button bg-red hover:bg-red_dark jq-youtube" href="#" target="_blank">
			<?php the_field('video_button', 8); ?>
		</a>

	</div>

</div>

<div class="youtube__wrap fixed inset-0 z-40">
	<a class="jq-youtube" href="#">
		✕
	</a>
	<div class="youtube__modal">
		<div class="youtube__box">
			<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe id="youtube-video" src='https://www.youtube.com/embed/<?php the_field('video_id', 8); ?>' frameborder='0' allowfullscreen></iframe></div>
		</div>
	</div>
</div>

<div class="bg-white py-16">

	<div class="container">

		<?php echo get_template_part( 'parts/already' ); ?>

	</div>

</div>

<?php get_footer(); ?>
