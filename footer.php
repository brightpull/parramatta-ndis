<div class="bg-charcoal py-12">

	<div class="container text-center lg:flex lg:justify-between">

        <div>

			<a href="https://www.parramattamission.org.au"><img class="inline-block footer__logo" src="<?php echo get_template_directory_uri(); ?>/svg/logo-footer.svg" alt="parramatta mission logo"></a>

			<p class="mb-0 mt-4 text-sm text-white"><a class="text-white" href="tel:1800512151">1800 512 151</a> | <a class="text-white" href="mailto:ndis@parramattamission.org.au">ndis@parramattamission.org.au</a></p>

		</div>

        <ul class="mb-0 hidden lg:flex items-center font-medium">
            <li class="block ml-12 uppercase text-xs">
				<a class="text-white hover:text-red" href="<?php echo get_permalink( 10 ); ?>">
					What We Do
				</a>
			</li>
            <li class="block ml-12 uppercase text-xs">
				<a class="text-white hover:text-red" href="<?php echo get_permalink( 12 ); ?>">
					The NDIS
				</a>
			</li>
            <li class="block ml-12 uppercase text-xs">
				<a class="text-white hover:text-red" href="<?php echo get_permalink( 14 ); ?>">
					FAQ
				</a>
			</li>
            <li class="block ml-12 uppercase text-xs">
				<a class="text-white hover:text-red" href="<?php echo get_permalink( 16 ); ?>">
					About Us
				</a>
			</li>
            <li class="block ml-12 uppercase text-xs">
				<a class="button bg-red hover:bg-red_dark" href="<?php echo get_permalink( 18 ); ?>">
					Get In Touch
				</a>
			</li>
        </ul>

    </div>

</div>

<footer class="bg-black py-4 text-white text-xs">

	<div class="container lg:flex justify-between text-center">

		<p class="mb-0">Website &copy; Parramatta Mission | <a class="text-white hover:text-red" href="https://www.parramattamission.org.au">Visit Main Website</a> | <a href="https://www.parramattamission.org.au/red">RED</a> | NDIS Provider Number 4050008357</p>

		<p class="mb-0">website by <a class="text-white hover:text-red" href="http://brightagency.com.au" target="_blank">bright</a></p>

	</div>

</footer>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-148611263-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-148611263-6');
</script>

<?php wp_footer(); ?>

</body>

</html>
