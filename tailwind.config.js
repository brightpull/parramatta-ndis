module.exports = {
  theme: {
    screens: {
      lg: '1100px',
    },
    colors: {
      black: '#000',
      blue: '#662c91',
      blue_dark: '#4d1378',
      charcoal: '#2c2c2c',
      green: '#38c172',
      red: '#a6192e',
      red_dark: '#bd2f37',
      shade: '#f4f4f4',
      white: '#fff',

      gray: {
        100: '#f7fafc',
        200: '#edf2f7',
        300: '#e2e8f0',
        400: '#cbd5e0',
        500: '#a0aec0',
        600: '#718096',
        700: '#4a5568',
        800: '#2d3748',
        900: '#1a202c',
      },
    },
    container: {
        center: true
    },
    fontFamily: {
      sans: [
        'Avenir',
        'sans-serif',
      ],
    },
  },
}
