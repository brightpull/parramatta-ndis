<?php

// Ancestor page ID
function bedrock_ancestor_id()
{
    global $post;
    if ( $post->post_parent ) {
        $ancestors = array_reverse( get_post_ancestors( $post->ID ) );
        return $ancestors[0];
    }
    return $post->ID;
}

// Hero background URL
function bedrock_hero_img_url()
{
    if ( is_single('post') )
    {
        return get_the_post_thumbnail_url( 127, 'full' );
    }

    global $post;

    // Does this post have a specific CPT header image?
    if ( get_field( 'header_image', $post->ID ) ) {
        return get_field( 'header_image', $post->ID );
    }

    // Does this post have a featured image?
    if ( get_the_post_thumbnail_url( $post->ID ) ) {
        return get_the_post_thumbnail_url( $post->ID, 'full' );
    }

    // Does it’s ancestor have one?
    if ( get_the_post_thumbnail_url( bedrock_ancestor_id() ) ) {
        return get_the_post_thumbnail_url( bedrock_ancestor_id(), 'full' );
    }

    // No? Fall back to default hero image
    return get_template_directory_uri() . '/images/hero-default.jpg';
}
