<?php

// Buttons
function bedrock_button_shortcode( $atts = array() )
{
	extract(shortcode_atts(array(
		'text' => 'Button Text',
		'link' => '#',
		'color' => 'blue',
	), $atts));
	return '<a href="' . $link . '" class="button mb-8 bg-' . $color . ' hover:bg-' . $color . '_dark">' . $text . '</a>';
}
add_shortcode('button', 'bedrock_button_shortcode');
