<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">

    <meta name="author" content="Tim Martin">

    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/e5de62e14f.js"></script>

    <!-- Google Translate -->
    <script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">

    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php wp_body_open(); ?>

<div class="bg-red_dark text-white text-xs py-4 lg:py-3">

    <div class="container flex justify-between font-semibold">

        <p class="mb-0 flex items-center text-base">
            <a href="tel:1800512151"><i class="fas fa-phone mr-2 text-xl lg:text-xs"></i> CALL US </a>
            <span class="hidden lg:inline">&nbsp;ON <a href="tel:1800512151">1800 512 151</a></span>
        </p>

        <div class="flex items-center">

            <div class="hidden lg:block" id="google_translate_element"></div>

            <a class="button--small bg-red hover:bg-red ml-4 hidden lg:inline-block" href="https://talkify.net/text-to-speech" target="_blank" title="Read this website aloud">Talkify</a>

        </div>

    </div>

</div>

<header class="bg-white py-12">

    <div class="container flex justify-between items-center">

        <a href="<?php echo get_permalink( 8 ); ?>">
            <img class="header__logo" src="<?php echo get_template_directory_uri(); ?>/svg/logos-header.svg" alt="parramatta mission logo">
        </a>

        <ul class="font-semibold mb-0 hidden lg:flex items-center">
            <li class="block text-sm">
                <a href="<?php echo get_permalink( 8 ); ?>">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li class="block ml-8 text-sm">
                <a class="has-dropdown uppercase block" href="<?php echo get_permalink( 10 ); ?>">
                    What We Do<i class="ml-2 fas text-gray-600 text-xs fa-chevron-down"></i>
                </a>
                <ul class="dropdown">
                    <?php if ( have_rows('what_we_do_links', 8) ) : ?>
                        <?php while ( have_rows('what_we_do_links', 8) ) : the_row(); ?>
                            <li><a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('text'); ?></a></li>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </li>
            <li class="block ml-8 text-sm">
                <a class="has-dropdown uppercase block" href="<?php echo get_permalink( 12 ); ?>">
                    The NDIS<i class="ml-2 fas text-gray-600 text-xs fa-chevron-down"></i>
                </a>
                <ul class="dropdown">
                    <?php if ( have_rows('the_ndis', 8) ) : ?>
                        <?php while ( have_rows('the_ndis', 8) ) : the_row(); ?>
                            <li><a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('text'); ?></a></li>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </li>
            <li class="block ml-8 text-sm">
                <a class="uppercase block" href="<?php echo get_permalink( 14 ); ?>">
                    FAQ
                </a>
            </li>
            <li class="block ml-8 text-sm">
                <a class="uppercase block" href="<?php echo get_permalink( 16 ); ?>">
                    About Us
                </a>
            </li>
            <li class="block ml-8 text-sm">
                <a class="uppercase block" href="<?php echo get_permalink( 127 ); ?>">
                    News
                </a>
            </li>
            <li class="block ml-8 uppercase text-sm">
                <a class="button bg-blue hover:bg-blue_dark" href="<?php echo get_permalink( 125 ); ?>">
                    Contact Us
                </a>
            </li>
        </ul>

        <a class="block lg:hidden js-mobile-menu text-red ml-6 text-center" href="#">
            <i class="block fas fa-bars text-3xl mb-3"></i>
            <div class="block text-xs font-semibold">
                MENU
            </div>
        </a>

    </div>

</header>

<nav class="hidden mobile-menu">
    <ul class="mb-0">
        <li>
            <a class="has-slidedown block font-medium p-4 bg-blue text-white leading-none uppercase" href="<?php echo get_permalink( 10 ); ?>">What We Do</a>
            <ul class="slidedown">
                <?php wp_list_pages('title_li=&depth=1&child_of=10'); ?>
            </ul>
        </li>
        <li>
            <a class="has-slidedown block font-medium p-4 bg-red text-white leading-none uppercase" href="<?php echo get_permalink( 12 ); ?>">The NDIS</a>
            <ul class="slidedown">
                <?php wp_list_pages('title_li=&depth=1&child_of=12'); ?>
            </ul>
        </li>
        <li><a class="block font-medium p-4 bg-blue text-white leading-none uppercase" href="<?php echo get_permalink( 14 ); ?>">FAQ</a></li>
        <li><a class="block font-medium p-4 bg-red text-white leading-none uppercase" href="<?php echo get_permalink( 16 ); ?>">About Us</a></li>
        <li><a class="block font-medium p-4 bg-blue text-white leading-none uppercase" href="<?php echo get_permalink( 125 ); ?>">Contact Us</a></li>
    </ul>
</nav>
